#include <stdio.h>
#include <string.h>
#define words 2
int main(int argc, char const *argv[]) {
  int c,i,n = words;
  char arrayStr[words][20], aux[20];
  printf(" Ingrese dos palabras:\n");
  for(i = 0; i < words; i++){
    printf(" Palabra(%d): ", i);
    scanf("%s", arrayStr[i]);
  }
  while(c != n-1){
    c = 0;
    for(i = 1; i < n; i++){
        if(strcmp(arrayStr[i],arrayStr[i-1]) < 0){
          strcpy(aux,arrayStr[i]);
          strcpy(arrayStr[i],arrayStr[i-1]);
          strcpy(arrayStr[i-1],aux);
        } else {c++;}
    }
  }
  printf(" Palabras en orden alfabetico: \n");
  for (i = 0; i < words; i++) {
    printf("  - %s [%d]\n", arrayStr[i], strlen(arrayStr[i]));
  }
  return 0;
}
